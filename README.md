Weather App Dashboard
===========
####Showcase
[Weather Iserv](http://weather.iserv.fr)
default admin account  : username ```admin@admin.com``` and password ```password```.
default user  account  : username ```test@test.com``` and password ```$Talmac!```.

[Weather Iserv](http://weather.iserv.fr)

#### Virtualhost
````
<VirtualHost *:80>
    ServerAdmin mail@iserv.fr
    DocumentRoot /var/www/html/weather/public/
    ServerName weather.iserv.fr
    ErrorLog logs/weather.iser.fr-error_log
    CustomLog logs/weather.iserv.fr-access_log common
    <Directory /var/www/html/weather/public/>
   Options -Indexes
  Order allow,deny
  Allow from all        
AllowOverride All
        AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript
    </Directory>

</VirtualHost>
````

SlimStarter is a bootstrap application built with Slim Framework in MVC architecture,
with Laravel's Eloquent as database provider (Model) and Twig as template engine (View).

Additional package is Sentry as authentication provider and Slim-facade which provide easy access to underlying Slim API
with static interface like Laravel syntax (built based on Laravel's Facade).




####Installation


#####1 Manual Install

```
$composer install
```

#####3 Setup Permission
After composer finished install the dependencies, you need to change file and folder permission.
```
chmod -R 777 app/storage/
chmod 666 app/config/database.php
```

#####4 Configure and Setup Database
You can now access the installer by pointing install.php in your browser
```
http://localhost/path/to/SlimStarter/public/install.php
```

**Import weather_city.sql in the databse**

####Configuration
Configuration file of SlimStarter located in ```app/config```, edit the database.php, cookie.php and other to match your need


####Model
Models are located in ```app/models``` directory, since Eloquent is used as database provider, you can write model like you

####Controller
Controllers are located in ```app/controllers``` directory, you may extends the BaseController to get access to predefined helper.
You can also place your controller in namespace to group your controller.

file : app/controllers/HomeController.php

####View
Views file are located in ```app/views``` directory in twig format, there is master.twig with 'body' block as default master template
shipped with SlimStarer that will provide default access to published js variable.

For detailed Twig documentation, please refer to http://twig.sensiolabs.org/documentation


file : app/views/welcome.twig

```html
{% extends 'master.twig' %}
{% block body %}
    Welcome to SlimStarter
{% endblock %}

```
