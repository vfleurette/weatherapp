    if ($('#cities').length>0 ) {
       $.ajax({
            url: "/admin/loadcity",
            type : 'POST',
            dataType: "json",
            success: function(response){
                var data = $(response).map(function(){
                    return {theid: this.id, value: this.name_city, id_city: this.id_city, country: this.country  };
                }).get();
                $('#cities').autocomplete({
                    source: data,
                    minLength: 2,
                    select: function(event,ui){
                        var sid = ui.item.theid
                          //Get full prospect info
                          msg='';
                        $.ajax({
                            url: "/admin/pushnewcity",
                            data: {newid_city: sid},
                            type: "POST",
                            success: function(msg) {
                                if(msg == 1){
                                $('table tr:last').after("<tr data-id='"+ui.item.id_city+"'><td>"+ui.item.id_city+"</td><td>"+ui.item.value+"</td><td>"+ui.item.country+"</td><td><button  data-toggle='modal' data-target='#myremoveModal' class='btnDelete btn btn-danger'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td></tr>");
                                }else{
                                    message='<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>La ville existe deja ! </div>';
                $('.answer').html(message);
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                              alert("Error getting prospect list: " + textStatus + '---' + errorThrown + '===' + XMLHttpRequest);
                            }
                          });
                    }
                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                .append( "<a>" + item.value +", "+item.country+"</a>")
                .appendTo( ul );
                };
            }
        });
    }
$(function() {
    $('.btnDelete').on('click', function (e) {
        //e.preventDefault();
        var id = $(this).closest('tr').data('id');
        $('#myremoveModal').data('id', id);
    });
    $('#btnDelteYes').unbind('click').bind('click', function () {
        var id = $('#myremoveModal').data('id');
        $.ajax({
        type: "POST",
        data: {deletecitybyid: id},
        url: "/admin/deletecity",
        success: function(msg)
        {
            var message= '';
            if (msg == 'true'){
                message='<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>La ville a bien été supprimé !</div>';
            $('.answer').html(message);
            }else{
                message='<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Erreur lors de la suppression de la ville :( </div>';
                $('.answer').html(message);
            }
        }
        });
        $('[data-id=' + id + ']').remove();
    });
});