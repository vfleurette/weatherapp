<?php

/**
 * Sample group routing with user check in middleware
 */
Route::group(
    '/admin',
    function(){
        if(!Sentry::check()){

            if(Request::isAjax()){
                Response::headers()->set('Content-Type', 'application/json');
                Response::setBody(json_encode(
                    array(
                        'success'   => false,
                        'message'   => 'Session expired or unauthorized access.',
                        'code'      => 401
                    )
                ));
                App::stop();
            }else{
                $redirect = Request::getResourceUri();
                Response::redirect(App::urlFor('login').'?redirect='.base64_encode($redirect));
            }
        }
    },
    function() use ($app) {
        /** sample namespaced controller */
        Route::get('/', 'Admin\AdminController:index')->name('admin');
        Route::get('/manage', 'Admin\AdminController:managelocalisation')->name('manage');
        Route::post('/loadcity', 'Admin\AdminController:loadcity')->name('loadcity');;
        Route::post('/pushnewcity', 'Admin\AdminController:pushnewcity')->name('pushnewcity');;
        Route::post('/deletecity', 'Admin\AdminController:deletecity')->name('deletecity');;

        foreach (Module::getModules() as $module) {
            $module->registerAdminRoute();
        }
    }
);

Route::get('/login', 'Admin\AdminController:login')->name('login');
Route::get('/logout', 'Admin\AdminController:logout')->name('logout');
Route::post('/login', 'Admin\AdminController:doLogin');

foreach (Module::getModules() as $module) {
    $module->registerPublicRoute();
}

/** default routing */
Route::get('/', 'HomeController:welcome');
Route::get('/register', 'HomeController:register')->name('formRegister');
Route::post('/inscription', 'HomeController:doRegister')->name('doRegister');