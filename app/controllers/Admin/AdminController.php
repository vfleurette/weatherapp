<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Menu;
use \Module;
use \Sentry;
use \Response;
use \DB;
use Cmfcmf\OpenWeatherMap;
use Cmfcmf\OpenWeatherMap\Exception as OWMException;

class AdminController extends BaseController
{

    /**
     * display the admin dashboard
     */
    public function index()
    {
                $adminMenu = Menu::get('admin_sidebar');
                $adminMenu->setActiveMenu('dashboard');
                $user = Sentry::getUser();
        $this->data['usercity']
            = DB::table('user_to_city')
            ->join('weather_city', 'user_to_city.id_city', '=', 'weather_city.id')
            //->select('user_to_city.id_city')
            ->select('weather_city.name_city')
            ->where('id_user', $user['id'])
            ->get();

        // Language of data (try your own language here!):
        $lang = 'fr';
        // Units (can be 'metric' or 'imperial' [default]):
        $units = 'metric';
        // Get OpenWeatherMap object. Don't use caching (take a look into Example_Cache.php to see how it works).
        $dataweather=array();
        foreach ( $this->data['usercity']  as $namecity){
            //print_r($namecity['name_city']);
        $owm = new OpenWeatherMap();
            try {
                $weather = $owm->getWeather($namecity['name_city'], $units, $lang);
                $dataweather[]=$weather;
                 $this->data['dataweather']=$dataweather;
            } catch(OWMException $e) {
                echo 'OpenWeatherMap exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
                echo "<br />\n";
            } catch(\Exception $e) {
                echo 'General exception: ' . $e->getMessage() . ' (Code ' . $e->getCode() . ').';
                echo "<br />\n";
            }

        }
        //print_r($this->data['dataweather']);
        View::display('admin/index.twig', $this->data);
    }


    /**
     * display the mangage localisation
     */
    public function managelocalisation()
    {
        $adminMenu = Menu::get('admin_sidebar');
        $adminMenu->setActiveMenu('manageweather');
        $this->data['title'] ='Settings';
        $user = Sentry::getUser();
        $this->data['usercity']
            = DB::table('user_to_city')
            ->join('weather_city', 'user_to_city.id_city', '=', 'weather_city.id')
            ->select('user_to_city.id', 'weather_city.id_city', 'weather_city.name_city', 'weather_city.country')
            ->where('id_user', $user['id'])
            ->get();
        View::display('admin/manage.twig', $this->data);
    }

    /**
     * load the  city
     */
    public function loadcity()
    {
        $city= DB::table('weather_city')
            ->select('weather_city.id', 'id_city', 'name_city', 'country')
            ->where('country', 'FR')
            //->limit(100)
            ->get();
        $result= json_encode($city);
        echo $result;
    }

    /**
     * push the  new city
     */
    public function pushnewcity()
    {
        $testExist = \Usertocity::where(
            'id_city',
           Input::post('newid_city')
        )->where(
            'id_user',
            Sentry::getUser()['id']
        );
        if (($testExist->count() == 0)) {

            $newid_city           = Input::post('newid_city');
            $datacity           = new \Usertocity;
            $datacity->id_city = $newid_city;
            $datacity->id_user  = Sentry::getUser()['id'];
            $datacity->save();
            echo true;
        } else {
            echo false;
        }
    }

    /**
     * push the  new city
     */
    public function deletecity()
    {
        $testExist = \Usertocity::where(
            'id',
           Input::post('deletecitybyid')
        )->where(
            'id_user',
            Sentry::getUser()['id']
        );
        $id=Input::post('deletecitybyid');

        if ($testExist->count() > 0) {
             if (!empty($id)) {
                        try {
                            /* creation and save of Deletedfiles object */
                            $deletedcity = \Usertocity::where('id', $id);
                            $deletedcity->delete();
                        } catch ( \Exception $e ) {
                            echo json_encode('error supression sql');
                        }
                        echo 'true';
                    } else {
                        echo 'false';
                    }
        } else {
            App::flash('error', 'Vous avez deja supprimé cette ville !');
        }
    }

    /**
     * display the login form
     */
    public function login()
    {
        if(Sentry::check()){
            Response::redirect($this->siteUrl('admin'));
        }else{
            $this->data['redirect'] = (Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '';
            View::display('admin/login.twig', $this->data);
        }
    }

    /**
     * Process the login
     */
    public function doLogin()
    {
        $remember = Input::post('remember', false);
        $email    = Input::post('email');
        $redirect = Input::post('redirect');
        $redirect = ($redirect) ? $redirect : 'admin';

        try{
            $credential = array(
                'email'     => $email,
                'password'  => Input::post('password')
            );

            // Try to authenticate the user
            $user = Sentry::authenticate($credential, false);

            if($remember){
                Sentry::loginAndRemember($user);
            }else{
                Sentry::login($user, false);
            }

            Response::redirect($this->siteUrl($redirect));
        }catch(\Exception $e){
            App::flash('message', $e->getMessage());
            App::flash('email', $email);
            App::flash('redirect', $redirect);
            App::flash('remember', $remember);

            Response::redirect($this->siteUrl('login'));
        }
    }

    /**
     * Logout the user
     */
    public function logout()
    {
        Sentry::logout();

        Response::redirect($this->siteUrl('/'));
    }

}